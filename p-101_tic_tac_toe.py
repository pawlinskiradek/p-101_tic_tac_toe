# https://gitlab.com/pawlinskiradek/p-101_tic_tac_toe.git

# Game - Tic Tac Toe

import random

print("Tic Tac Toe", end="\n\n")

def reset_board():
    # Funkcja resetująca planszę do stanu początkowego
    reset = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
    return reset

def print_board(board):
    # Funkcja do wyświetlania aktualnego stanu planszy
    print(f" {board[0]} | {board[1]} | {board[2]}")
    print("-----------")
    print(f" {board[3]} | {board[4]} | {board[5]}")
    print("-----------")
    print(f" {board[6]} | {board[7]} | {board[8]}")
    print()

def win_check(board):
    # Funkcja sprawdzająca, czy któryś z graczy wygrał
    if (board[0] == board[1] == board[2]) or (board[3] == board[4] == board[5]) or (board[6] == board[7] == board[8]) or (board[0] == board[3] == board[6]) or (board[1] == board[4] == board[7]) or (board[2] == board[5] == board[8]) or (board[0] == board[4] == board[8]) or (board[2] == board[4] == board[6]):
        return True

def computer_move(board):
    # Funkcja odpowiedzialna za ruch komputera
    available_moves = [i for i in range(9) if board[i] != 'X' and board[i] != 'O']
    if available_moves:
        return random.choice(available_moves) + 1
    else:
        return None

def player_move(board, player):
    # Funkcja obsługująca ruch gracza
    while True:
        try:
            player_choice = input(f"Gracz {player}, podaj pozycję od 1 do 9 lub wpisz 'q' żeby wyjść: ").lower()
            if player_choice == "q":
                return None
            player_choice = int(player_choice)
            if player_choice in range(1, 10) and board[player_choice - 1] != 'X' and board[player_choice - 1] != 'O':
                return player_choice
            else:
                print("Nieprawidłowy ruch. Spróbuj ponownie.")
        except ValueError:
            print("Wprowadź liczbę.")

# Inicjalizacja planszy
board_1 = reset_board()
print_board(board_1)

# Wybór trybu gry
while True:
    try:
        game_type = input("Czy grasz z komputerem (k), czy z drugim graczem (d)? ").lower()
        if game_type not in ['k', 'd']:
            raise ValueError("Nieprawidłowy wybór. Wybierz 'k' lub 'd'.")
        break
    except ValueError as e:
        print(e)

# Logika gry w zależności od wybranego trybu
if game_type == "k":
    # Gra z komputerem
    while True:
        player_1_choice = player_move(board_1, "X")
        if player_1_choice is None:
            break
        board_1[player_1_choice - 1] = "X"
        if win_check(board_1):
            print_board(board_1)
            print("Gracz X wygrał!")
            break

        computer_choice = computer_move(board_1)
        if computer_choice is not None:
            print(f"Komputer wybiera pozycję {computer_choice}")
            board_1[computer_choice - 1] = "O"
            if win_check(board_1):
                print_board(board_1)
                print("Komputer wygrał!")
                break
        else:
            print("Remis!")
            break

        print_board(board_1)
elif game_type == "d":
    # Gra z drugim graczem
    while True:
        player_1_choice = player_move(board_1, "X")
        if player_1_choice is None:
            break
        board_1[player_1_choice - 1] = "X"
        if win_check(board_1):
            print_board(board_1)
            print("Gracz X wygrał!")
            break

        player_2_choice = player_move(board_1, "O")
        if player_2_choice is None:
            break
        board_1[player_2_choice - 1] = "O"
        if win_check(board_1):
            print_board(board_1)
            print("Gracz O wygrał!")
            break

        print_board(board_1)
else:
    print("Nieprawidłowy wybór. Wybierz 'k' lub 'd'.")
